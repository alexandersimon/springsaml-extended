# README #

Beispiel zur Kommunikation zwischen einer WebApp mit dem AD FS 3 unter Verwendung der Spring Libs.

### Konfigurationen ###

Folgende Konfigurationen müssen in der WEB-INF\securityContext.xml vorgenommen werden.

* Bekanntgabe des Keystores
~~~~
    <!-- Central storage of cryptographic keys -->
    <bean id="keyManager" class="org.springframework.security.saml.key.JKSKeyManager">
        <constructor-arg value="classpath:security/samlKeystore.jks"/>
        <constructor-arg type="java.lang.String" value="abab000a"/> //Das ist das Kennwort für den Keystore
        <constructor-arg>
            <map>
                <entry key="wildfly" value="abab000a"/> //Das ist das Kennwort für den Privatekey
            </map>
        </constructor-arg>
        <constructor-arg type="java.lang.String" value="wildfly"/>
    </bean>
~~~~
* Für den die Generierung der metadata.xml müssen diese Angaben übereinstimmen
~~~~
	<bean id="metadataGeneratorFilter"
		class="org.springframework.security.saml.metadata.MetadataGeneratorFilter">
		<constructor-arg>
			<bean class="org.springframework.security.saml.metadata.MetadataGenerator">
				<property name="entityId" value="urn:sso-test:springsaml:com" />
				<property name="requestSigned" value="true" />

				<property name="extendedMetadata">
					<bean class="org.springframework.security.saml.metadata.ExtendedMetadata">
						<property name="local" value="false" />
						<property name="requireArtifactResolveSigned" value="false" />
						<property name="requireLogoutRequestSigned" value="true" />
						<property name="requireLogoutResponseSigned" value="true" />

						<property name="signMetadata" value="true" />
						<property name="signingKey" value="wildfly" /> //Alias für den PrivateKey
						<property name="idpDiscoveryEnabled" value="false" />
					</bean>
				</property>

			</bean>
		</constructor-arg>
	</bean>
~~~~