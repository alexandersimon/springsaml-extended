<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <link rel="icon" type="image/png" href="<c:url value="/images/favicon.png"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css"/>" media="screen" />
    <title>Spring SAML Login</title>
</head>